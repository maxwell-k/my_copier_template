# SPDX-License-Identifier: MPL-2.0
# SPDX-FileCopyrightText: 2023 Keith Maxwell
from pathlib import Path
from shutil import rmtree

import nox

VIRTUAL_ENVIRONMENT = ".venv"
PYTHON = Path(VIRTUAL_ENVIRONMENT).absolute() / "bin" / "python"
PYTHON_VERSION = "3.12"

nox.options.sessions = [
    "example",
    "reuse",
]


@nox.session()
def bumpver(session) -> None:
    """Increment the version number"""
    session.install("bumpver")
    session.run("bumpver", "update", *session.posargs)


@nox.session(python=False)
def dev(session) -> None:
    """Set up a virtual environment"""
    rmtree(VIRTUAL_ENVIRONMENT, ignore_errors=True)
    session.run(
        f"python{PYTHON_VERSION}",
        "-m",
        "venv",
        "--upgrade-deps",
        VIRTUAL_ENVIRONMENT,
    )
    session.run(
        PYTHON,
        "-m",
        "pip",
        "install",
        "--use-pep517",
        "black",
        "copier",
        "flake8",
        "flit",
        "nox",
        "reorder-python-imports",
        "reuse",
    )


@nox.session()
def example(session) -> None:
    """Interactively render an example project"""
    example = Path("example")
    rmtree(example, ignore_errors=True)
    session.install("copier")
    session.run(
        "copier",
        "copy",
        "--trust",
        "--vcs-ref=HEAD",
        "--defaults",
        "--data=docstring=Example docstring",
        Path.cwd(),
        example,
    )


@nox.session()
def reuse(session) -> None:
    """Check the template project against the reuse specification"""
    session.install("reuse")
    session.run("reuse", "lint")
