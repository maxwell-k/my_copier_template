# my_copier_template

## Quick start

Use with [copier] and [pipx]:

    pipx run copier copy --trust gl:maxwell-k/my_copier_template example

This command creates a module and package named `example` that uses:

- [black]
- [flake8]
- [flit], with today's date as the first version number
- [nox]
- [reorder-python-imports]
- [reuse]
- a [src layout]
- the Mozilla Public License, Version 2.0

## Alternatives

If the package is going to include source code in a compiled language such as C
it may be helpful to switch to [setuptools] in place of Flit.

## Template Development

Command to start with a local clone of this repository:

    git clone git@gitlab.com:maxwell-k/my_copier_template.git \
    && cd my_copier_template

Then commit your proposed changes.

Command to manually test rendering a new project:

    nox --session=example

Command to bump the version number:

    nox --session=bumpver -- --dry

## Why use version numbers? Why use bumpver?

Think of this repository or project as a template. Call another repository that
is created from this project a [subproject]. Copier supports [updating] the
subproject to incorporate changes to the template. In order to update a
subproject copier needs to understand different versions of the template. A
system of git tags is used so that copier can compare version numbers and update
subprojects. [bumpver] provides automation for version numbering.

## Maintenance

Command to check the reuse status of this repository:

    nox --session=reuse

[black]: https://github.com/psf/black
[flake8]: https://flake8.pycqa.org/en/latest/
[flit]: https://flit.pypa.io/en/stable/
[pipx]: https://pypa.github.io/pipx/
[reorder-python-imports]: https://github.com/asottile/reorder_python_imports
[setuptools]: https://setuptools.pypa.io/en/latest/
[src layout]:
  https://packaging.python.org/en/latest/discussions/src-layout-vs-flat-layout/
[copier]: https://copier.readthedocs.io/en/stable/
[updating]: https://copier.readthedocs.io/en/stable/updating/
[bumpver]: https://github.com/mbarkhau/bumpver
[subproject]: https://copier.readthedocs.io/en/stable/reference/subproject/
[reuse]: https://reuse.software/
[nox]: https://nox.thea.codes/en/stable/
